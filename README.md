# Testing-Guide

## Tests kompilieren und durchlaufen lassen

Kompilieren und starten:

```
$ gulp fulltest
```

Starten ohne zu kompilieren:

```
$ gulp test
```

## Tagging

Angelehnt an das praktische
[Tagging von Cucumber](https://github.com/cucumber/cucumber/wiki/Tags)
können wir Mochas `--grep`-Option nutzen, um einen ähnlichen Effekt zu erzielen.
Zu diesem Zweck wird ein an den Gulp-Aufruf übergebenes `--grep`-Argument an
Mocha weitergereicht.

Das durch Cucumbers `--tags`-Argument hervorgerufene Verhalten kann bei der
Verwendung von Mocha mittels `--grep` annähernd erreicht werden, da Letzteres
mit regulären Ausdrücken arbeitet. Logische Verknüpfungen können folgendermaßen
notiert werden:

### Und-Verknüpfung von Tags

Cucumber-Vorbild:

```
$ cucumber --tags @sometag --tags @someothertag
```

Mocha (via Gulp):

```
$ gulp test --grep "@sometag @someothertag"
```

Das geht leider nur dann, wenn die Tags in exakt der richtigen Reihenfolge
angegeben werden, wie sie auch in der Testbeschreibung stehen.
`gulp test --grep "(?=@abc)(?=@notbuggy)"` versteht Mocha leider nicht
(Lösung gesucht, bitte um Vorschläge).

### Oder-Verknüpfung von Tags

Cucumber-Vorbild:

```
$ cucumber --tags @sometag,@someothertag
```

Mocha (via Gulp):

```
$ gulp test --grep "(@sometag|@someothertag)"
```


## Für das Testen relevante Dateien

| Dateien                                   | Beschreibung                                                              |
|-------------------------------------------|---------------------------------------------------------------------------|
| `gulp/testing.js`                         | Hauptkonfiguration der Testtasks                                          |
| `gulp/lib/testing/database.js`            | Verbindungsaufbau zur Testdatenbank, Löschen der Datenbank für jeden Test |
| `src/commons/testing/mocha/asyncAwait.ts` | Async/Await-Wrapper für Mocha                                             |
| `src/config/env/config.ts`                | Konfiguration der `fixtureDb` (MongoDB für das Testing)                   |
| `modules/*/spec/service/*Spec.ts`         | klassische Unit-Tests, welche einzelne Servicefunktionen testen           |
| `modules/*/spec/routes/*Spec.ts`          | Tests, die HTTP-vermittelt testen                                         |


## 3-A-Struktur

 1. Arrange
 2. Act
 3. Assert

Literatur:

 * http://xp123.com/articles/3a-arrange-act-assert
 * http://www.bymichaellancaster.com/blog/client-side-javascript-unit-tests-for-front-end-developers


## Klassische Unit-Tests

Beispiel: `src/modules/account/spec/service/accountSpec.ts`

Die Zeile `let expect = require('chai').expect;` lädt Chais `expect`, womit
das dritte A ("Assert") bedient werden kann.


## Tests via HTTP

Beispiele: `src/modules/auth/spec/routes/authSpec.ts`

Besonderheit: `import * as request from 'supertest';` (für das Erzeugen
von HTTP-Anfragen, und das Überprüfen der Resultate dieser Abfragen (z.B. `.expect(200)`))

## Bekannte Probleme

Für die folgenden Probleme würde ich gerne Lösungen finden und freue mich über
Lösungsvorschläge:

 * Tagging funktioniert nicht ganz so schön wie mit Cucumber (siehe oben).
 * Ohne `require('babel-polyfill');` in den *Spec.ts-Dateien läuft es nicht.
